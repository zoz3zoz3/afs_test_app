import 'package:afs_test_app/pages/image_clicker_page.dart';
import 'package:afs_test_app/utils/context_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChooseImagesCount extends StatefulWidget {
  const ChooseImagesCount({super.key});

  @override
  State<ChooseImagesCount> createState() => _ChooseImagesCountState();
}

class _ChooseImagesCountState extends State<ChooseImagesCount> {
  int chosenCount = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Choose Images Count'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: TextFormField(
                onChanged: (value) => setState(() {
                  if (value.isNotEmpty) chosenCount = int.parse(value);
                }),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(2),
                ],
                decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            OutlinedButton(
              onPressed: () {
                if (chosenCount != 0) {
                  context.pushPage(ImageClickerPage(imagesCount: chosenCount));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Images Count should be positive number'),
                    ),
                  );
                }
              },
              style: OutlinedButton.styleFrom(
                side: BorderSide(
                  color: context.theme.primaryColor,
                ),
                fixedSize: const Size(100, 50),
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: context.theme.primaryColor,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
              ),
              child: const Text('Next'),
            )
          ],
        ),
      ),
    );
  }
}
