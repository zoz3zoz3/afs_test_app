import 'package:afs_test_app/models/clickable_image_model.dart';
import 'package:afs_test_app/utils/context_extension.dart';
import 'package:afs_test_app/viewmodel/image_clicker_viewmodel.dart';
import 'package:afs_test_app/widgets/image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';

class ImageClickerPage extends StatefulWidget {
  const ImageClickerPage({
    super.key,
    required this.imagesCount,
  });
  final int imagesCount;

  @override
  State<ImageClickerPage> createState() => _ImageClickerPageState();
}

class _ImageClickerPageState extends State<ImageClickerPage> {
  late ImageClickerViewmodel viewmodel;
  @override
  void initState() {
    viewmodel = GetIt.I<ImageClickerViewmodel>(param1: widget.imagesCount);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Click the Image to inlarge it'),
      ),
      body: Observer(
        builder: (_) {
          return SingleChildScrollView(
            child: Container(
              width: 1.sw,
              height: 3.sh,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.redAccent,
                ),
              ),
              child: Stack(
                children: [
                  ...List.generate(
                    viewmodel.images.length,
                    (index) {
                      final im = viewmodel.images[index];
                      return AnimatedPositioned(
                        duration: const Duration(milliseconds: 400),
                        top: im.placementModel.top,
                        left: im.placementModel.left,
                        width: im.dimensionsModel.width,
                        height: im.dimensionsModel.height,
                        child: ImageWidget(
                          index: index,
                          model: im,
                          onTab: () => viewmodel.clickImage(im.id),
                        ),
                      );
                    },
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Container(
                      color: Colors.black38,
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Text(
                            'Total Area ${viewmodel.totlaScreenArea}',
                            style: context.textTheme.labelLarge,
                          ),
                          Text(
                            'Total images Area ${viewmodel.totlaImgesArea}',
                            style: context.textTheme.labelLarge,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}


//  Wrap(
//                 spacing: 0,
//                 runSpacing: 0,
//                 alignment: WrapAlignment.start,
//                 runAlignment: WrapAlignment.start,
//                 children: viewmodel.images
//                     .sortListByArea()
//                     .map((im) => ImageWidget(
//                           model: im,
//                           onTab: () => viewmodel.clickImage(im.id),
//                         ))
//                     .toList(),
//               ),