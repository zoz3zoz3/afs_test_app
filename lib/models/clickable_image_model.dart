import 'dart:ui';

import 'package:afs_test_app/models/dimensions_model.dart';
import 'package:afs_test_app/models/placement_model.dart';
import 'package:afs_test_app/utils/constants.dart';
import 'package:afs_test_app/utils/count_generator.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'clickable_image_model.freezed.dart';

@freezed
class ClickableImageModel with _$ClickableImageModel {
  const factory ClickableImageModel({
    required String id,
    required int clicksCount,
    required AspectRatioModel aspectRatioModel,
    
    required Color color,
    @Default(DimensionsModel()) DimensionsModel dimensionsModel,
    @Default(PlacementModel()) PlacementModel placementModel,
  }) = _ClickableImageModel;

  const ClickableImageModel._();

  double get area => dimensionsModel.area;
}

extension ClickableImageModelListExt on List<ClickableImageModel> {
  int calculateTotalClicksCount() {
    int totalClicks = fold(0, (int total, ClickableImageModel currentImage) => total + currentImage.clicksCount);
    return totalClicks;
  }
  double calculateTotalImagesArea() {
    double totalArea = fold<double>(0.0, (double total, ClickableImageModel currentImage) => total + currentImage.area);
    return totalArea;
  }

  List<ClickableImageModel> sortListByArea() {
    List<ClickableImageModel> res = [...this];
    res.sort((a, b) => b.area.compareTo(a.area));
    return res;
  }

}




  // List<ClickableImageModel> getSquares() {
  //   return where((it) => it.aspectRatioModel.isSquare).toList();
  // }

  // List<ClickableImageModel> getHorizontal() {
  //   return where((it) => it.aspectRatioModel.isHorizontal).toList();
  // }

  // List<ClickableImageModel> getVertical() {
  //   return where((it) => it.aspectRatioModel.isVertical).toList();
  // }

  // List<ClickableImageModel> get43({required bool isVertical}) {
  //   return isVertical
  //       ? getVertical().where((it) => it.aspectRatioModel.aspectRatioX == 3).toList()
  //       : getHorizontal().where((it) => it.aspectRatioModel.aspectRatioX == 4).toList();
  // }

  // List<ClickableImageModel> get32({required bool isVertical}) {
  //   return isVertical
  //       ? getVertical().where((it) => it.aspectRatioModel.aspectRatioX == 2).toList()
  //       : getHorizontal().where((it) => it.aspectRatioModel.aspectRatioX == 3).toList();
  // }

  // List<ClickableImageModel> get169({required bool isVertical}) {
  //   return isVertical
  //       ? getVertical().where((it) => it.aspectRatioModel.aspectRatioX == 9).toList()
  //       : getHorizontal().where((it) => it.aspectRatioModel.aspectRatioX == 16).toList();
  // }