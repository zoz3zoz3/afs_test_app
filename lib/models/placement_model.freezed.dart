// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'placement_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PlacementModel {
  double get top => throw _privateConstructorUsedError;
  double get left => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PlacementModelCopyWith<PlacementModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlacementModelCopyWith<$Res> {
  factory $PlacementModelCopyWith(
          PlacementModel value, $Res Function(PlacementModel) then) =
      _$PlacementModelCopyWithImpl<$Res, PlacementModel>;
  @useResult
  $Res call({double top, double left});
}

/// @nodoc
class _$PlacementModelCopyWithImpl<$Res, $Val extends PlacementModel>
    implements $PlacementModelCopyWith<$Res> {
  _$PlacementModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? top = null,
    Object? left = null,
  }) {
    return _then(_value.copyWith(
      top: null == top
          ? _value.top
          : top // ignore: cast_nullable_to_non_nullable
              as double,
      left: null == left
          ? _value.left
          : left // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlacementModelCopyWith<$Res>
    implements $PlacementModelCopyWith<$Res> {
  factory _$$_PlacementModelCopyWith(
          _$_PlacementModel value, $Res Function(_$_PlacementModel) then) =
      __$$_PlacementModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double top, double left});
}

/// @nodoc
class __$$_PlacementModelCopyWithImpl<$Res>
    extends _$PlacementModelCopyWithImpl<$Res, _$_PlacementModel>
    implements _$$_PlacementModelCopyWith<$Res> {
  __$$_PlacementModelCopyWithImpl(
      _$_PlacementModel _value, $Res Function(_$_PlacementModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? top = null,
    Object? left = null,
  }) {
    return _then(_$_PlacementModel(
      top: null == top
          ? _value.top
          : top // ignore: cast_nullable_to_non_nullable
              as double,
      left: null == left
          ? _value.left
          : left // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_PlacementModel extends _PlacementModel {
  const _$_PlacementModel({this.top = 0.0, this.left = 0.0}) : super._();

  @override
  @JsonKey()
  final double top;
  @override
  @JsonKey()
  final double left;

  @override
  String toString() {
    return 'PlacementModel(top: $top, left: $left)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlacementModel &&
            (identical(other.top, top) || other.top == top) &&
            (identical(other.left, left) || other.left == left));
  }

  @override
  int get hashCode => Object.hash(runtimeType, top, left);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlacementModelCopyWith<_$_PlacementModel> get copyWith =>
      __$$_PlacementModelCopyWithImpl<_$_PlacementModel>(this, _$identity);
}

abstract class _PlacementModel extends PlacementModel {
  const factory _PlacementModel({final double top, final double left}) =
      _$_PlacementModel;
  const _PlacementModel._() : super._();

  @override
  double get top;
  @override
  double get left;
  @override
  @JsonKey(ignore: true)
  _$$_PlacementModelCopyWith<_$_PlacementModel> get copyWith =>
      throw _privateConstructorUsedError;
}
