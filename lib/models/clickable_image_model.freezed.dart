// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'clickable_image_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ClickableImageModel {
  String get id => throw _privateConstructorUsedError;
  int get clicksCount => throw _privateConstructorUsedError;
  AspectRatioModel get aspectRatioModel => throw _privateConstructorUsedError;
  Color get color => throw _privateConstructorUsedError;
  DimensionsModel get dimensionsModel => throw _privateConstructorUsedError;
  PlacementModel get placementModel => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ClickableImageModelCopyWith<ClickableImageModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClickableImageModelCopyWith<$Res> {
  factory $ClickableImageModelCopyWith(
          ClickableImageModel value, $Res Function(ClickableImageModel) then) =
      _$ClickableImageModelCopyWithImpl<$Res, ClickableImageModel>;
  @useResult
  $Res call(
      {String id,
      int clicksCount,
      AspectRatioModel aspectRatioModel,
      Color color,
      DimensionsModel dimensionsModel,
      PlacementModel placementModel});

  $AspectRatioModelCopyWith<$Res> get aspectRatioModel;
  $DimensionsModelCopyWith<$Res> get dimensionsModel;
  $PlacementModelCopyWith<$Res> get placementModel;
}

/// @nodoc
class _$ClickableImageModelCopyWithImpl<$Res, $Val extends ClickableImageModel>
    implements $ClickableImageModelCopyWith<$Res> {
  _$ClickableImageModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? clicksCount = null,
    Object? aspectRatioModel = null,
    Object? color = null,
    Object? dimensionsModel = null,
    Object? placementModel = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      clicksCount: null == clicksCount
          ? _value.clicksCount
          : clicksCount // ignore: cast_nullable_to_non_nullable
              as int,
      aspectRatioModel: null == aspectRatioModel
          ? _value.aspectRatioModel
          : aspectRatioModel // ignore: cast_nullable_to_non_nullable
              as AspectRatioModel,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color,
      dimensionsModel: null == dimensionsModel
          ? _value.dimensionsModel
          : dimensionsModel // ignore: cast_nullable_to_non_nullable
              as DimensionsModel,
      placementModel: null == placementModel
          ? _value.placementModel
          : placementModel // ignore: cast_nullable_to_non_nullable
              as PlacementModel,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $AspectRatioModelCopyWith<$Res> get aspectRatioModel {
    return $AspectRatioModelCopyWith<$Res>(_value.aspectRatioModel, (value) {
      return _then(_value.copyWith(aspectRatioModel: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $DimensionsModelCopyWith<$Res> get dimensionsModel {
    return $DimensionsModelCopyWith<$Res>(_value.dimensionsModel, (value) {
      return _then(_value.copyWith(dimensionsModel: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PlacementModelCopyWith<$Res> get placementModel {
    return $PlacementModelCopyWith<$Res>(_value.placementModel, (value) {
      return _then(_value.copyWith(placementModel: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ClickableImageModelCopyWith<$Res>
    implements $ClickableImageModelCopyWith<$Res> {
  factory _$$_ClickableImageModelCopyWith(_$_ClickableImageModel value,
          $Res Function(_$_ClickableImageModel) then) =
      __$$_ClickableImageModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      int clicksCount,
      AspectRatioModel aspectRatioModel,
      Color color,
      DimensionsModel dimensionsModel,
      PlacementModel placementModel});

  @override
  $AspectRatioModelCopyWith<$Res> get aspectRatioModel;
  @override
  $DimensionsModelCopyWith<$Res> get dimensionsModel;
  @override
  $PlacementModelCopyWith<$Res> get placementModel;
}

/// @nodoc
class __$$_ClickableImageModelCopyWithImpl<$Res>
    extends _$ClickableImageModelCopyWithImpl<$Res, _$_ClickableImageModel>
    implements _$$_ClickableImageModelCopyWith<$Res> {
  __$$_ClickableImageModelCopyWithImpl(_$_ClickableImageModel _value,
      $Res Function(_$_ClickableImageModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? clicksCount = null,
    Object? aspectRatioModel = null,
    Object? color = null,
    Object? dimensionsModel = null,
    Object? placementModel = null,
  }) {
    return _then(_$_ClickableImageModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      clicksCount: null == clicksCount
          ? _value.clicksCount
          : clicksCount // ignore: cast_nullable_to_non_nullable
              as int,
      aspectRatioModel: null == aspectRatioModel
          ? _value.aspectRatioModel
          : aspectRatioModel // ignore: cast_nullable_to_non_nullable
              as AspectRatioModel,
      color: null == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color,
      dimensionsModel: null == dimensionsModel
          ? _value.dimensionsModel
          : dimensionsModel // ignore: cast_nullable_to_non_nullable
              as DimensionsModel,
      placementModel: null == placementModel
          ? _value.placementModel
          : placementModel // ignore: cast_nullable_to_non_nullable
              as PlacementModel,
    ));
  }
}

/// @nodoc

class _$_ClickableImageModel extends _ClickableImageModel {
  const _$_ClickableImageModel(
      {required this.id,
      required this.clicksCount,
      required this.aspectRatioModel,
      required this.color,
      this.dimensionsModel = const DimensionsModel(),
      this.placementModel = const PlacementModel()})
      : super._();

  @override
  final String id;
  @override
  final int clicksCount;
  @override
  final AspectRatioModel aspectRatioModel;
  @override
  final Color color;
  @override
  @JsonKey()
  final DimensionsModel dimensionsModel;
  @override
  @JsonKey()
  final PlacementModel placementModel;

  @override
  String toString() {
    return 'ClickableImageModel(id: $id, clicksCount: $clicksCount, aspectRatioModel: $aspectRatioModel, color: $color, dimensionsModel: $dimensionsModel, placementModel: $placementModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ClickableImageModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.clicksCount, clicksCount) ||
                other.clicksCount == clicksCount) &&
            (identical(other.aspectRatioModel, aspectRatioModel) ||
                other.aspectRatioModel == aspectRatioModel) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.dimensionsModel, dimensionsModel) ||
                other.dimensionsModel == dimensionsModel) &&
            (identical(other.placementModel, placementModel) ||
                other.placementModel == placementModel));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, clicksCount,
      aspectRatioModel, color, dimensionsModel, placementModel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ClickableImageModelCopyWith<_$_ClickableImageModel> get copyWith =>
      __$$_ClickableImageModelCopyWithImpl<_$_ClickableImageModel>(
          this, _$identity);
}

abstract class _ClickableImageModel extends ClickableImageModel {
  const factory _ClickableImageModel(
      {required final String id,
      required final int clicksCount,
      required final AspectRatioModel aspectRatioModel,
      required final Color color,
      final DimensionsModel dimensionsModel,
      final PlacementModel placementModel}) = _$_ClickableImageModel;
  const _ClickableImageModel._() : super._();

  @override
  String get id;
  @override
  int get clicksCount;
  @override
  AspectRatioModel get aspectRatioModel;
  @override
  Color get color;
  @override
  DimensionsModel get dimensionsModel;
  @override
  PlacementModel get placementModel;
  @override
  @JsonKey(ignore: true)
  _$$_ClickableImageModelCopyWith<_$_ClickableImageModel> get copyWith =>
      throw _privateConstructorUsedError;
}
