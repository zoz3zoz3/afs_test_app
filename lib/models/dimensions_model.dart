import 'package:freezed_annotation/freezed_annotation.dart';

part 'dimensions_model.freezed.dart';

@freezed
class AspectRatioModel with _$AspectRatioModel {
  const factory AspectRatioModel({
    required int aspectRatioX,
    required int aspectRatioY,
  }) = _AspectRatioModel;

  const AspectRatioModel._();

  bool get isHorizontal => aspectRatioX > aspectRatioY;
  bool get isVertical => aspectRatioX < aspectRatioY;
  bool get isSquare => aspectRatioX == aspectRatioY;
  double get aspectRatio => aspectRatioX / aspectRatioY;

  bool get isSmallRatio => aspectRatioX < 9 || aspectRatioY < 9;
}

@freezed
class DimensionsModel with _$DimensionsModel {
  const factory DimensionsModel({
    @Default(0.0) double width,
    @Default(0.0) double height,
  }) = _DimensionsModel;

  const DimensionsModel._();

  double get area => width * height;
}



class FrameSize {
  final double width;
  final double height;

  FrameSize(this.width, this.height);
}