// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dimensions_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AspectRatioModel {
  int get aspectRatioX => throw _privateConstructorUsedError;
  int get aspectRatioY => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AspectRatioModelCopyWith<AspectRatioModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AspectRatioModelCopyWith<$Res> {
  factory $AspectRatioModelCopyWith(
          AspectRatioModel value, $Res Function(AspectRatioModel) then) =
      _$AspectRatioModelCopyWithImpl<$Res, AspectRatioModel>;
  @useResult
  $Res call({int aspectRatioX, int aspectRatioY});
}

/// @nodoc
class _$AspectRatioModelCopyWithImpl<$Res, $Val extends AspectRatioModel>
    implements $AspectRatioModelCopyWith<$Res> {
  _$AspectRatioModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? aspectRatioX = null,
    Object? aspectRatioY = null,
  }) {
    return _then(_value.copyWith(
      aspectRatioX: null == aspectRatioX
          ? _value.aspectRatioX
          : aspectRatioX // ignore: cast_nullable_to_non_nullable
              as int,
      aspectRatioY: null == aspectRatioY
          ? _value.aspectRatioY
          : aspectRatioY // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AspectRatioModelCopyWith<$Res>
    implements $AspectRatioModelCopyWith<$Res> {
  factory _$$_AspectRatioModelCopyWith(
          _$_AspectRatioModel value, $Res Function(_$_AspectRatioModel) then) =
      __$$_AspectRatioModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int aspectRatioX, int aspectRatioY});
}

/// @nodoc
class __$$_AspectRatioModelCopyWithImpl<$Res>
    extends _$AspectRatioModelCopyWithImpl<$Res, _$_AspectRatioModel>
    implements _$$_AspectRatioModelCopyWith<$Res> {
  __$$_AspectRatioModelCopyWithImpl(
      _$_AspectRatioModel _value, $Res Function(_$_AspectRatioModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? aspectRatioX = null,
    Object? aspectRatioY = null,
  }) {
    return _then(_$_AspectRatioModel(
      aspectRatioX: null == aspectRatioX
          ? _value.aspectRatioX
          : aspectRatioX // ignore: cast_nullable_to_non_nullable
              as int,
      aspectRatioY: null == aspectRatioY
          ? _value.aspectRatioY
          : aspectRatioY // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_AspectRatioModel extends _AspectRatioModel {
  const _$_AspectRatioModel(
      {required this.aspectRatioX, required this.aspectRatioY})
      : super._();

  @override
  final int aspectRatioX;
  @override
  final int aspectRatioY;

  @override
  String toString() {
    return 'AspectRatioModel(aspectRatioX: $aspectRatioX, aspectRatioY: $aspectRatioY)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AspectRatioModel &&
            (identical(other.aspectRatioX, aspectRatioX) ||
                other.aspectRatioX == aspectRatioX) &&
            (identical(other.aspectRatioY, aspectRatioY) ||
                other.aspectRatioY == aspectRatioY));
  }

  @override
  int get hashCode => Object.hash(runtimeType, aspectRatioX, aspectRatioY);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AspectRatioModelCopyWith<_$_AspectRatioModel> get copyWith =>
      __$$_AspectRatioModelCopyWithImpl<_$_AspectRatioModel>(this, _$identity);
}

abstract class _AspectRatioModel extends AspectRatioModel {
  const factory _AspectRatioModel(
      {required final int aspectRatioX,
      required final int aspectRatioY}) = _$_AspectRatioModel;
  const _AspectRatioModel._() : super._();

  @override
  int get aspectRatioX;
  @override
  int get aspectRatioY;
  @override
  @JsonKey(ignore: true)
  _$$_AspectRatioModelCopyWith<_$_AspectRatioModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DimensionsModel {
  double get width => throw _privateConstructorUsedError;
  double get height => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DimensionsModelCopyWith<DimensionsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DimensionsModelCopyWith<$Res> {
  factory $DimensionsModelCopyWith(
          DimensionsModel value, $Res Function(DimensionsModel) then) =
      _$DimensionsModelCopyWithImpl<$Res, DimensionsModel>;
  @useResult
  $Res call({double width, double height});
}

/// @nodoc
class _$DimensionsModelCopyWithImpl<$Res, $Val extends DimensionsModel>
    implements $DimensionsModelCopyWith<$Res> {
  _$DimensionsModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? width = null,
    Object? height = null,
  }) {
    return _then(_value.copyWith(
      width: null == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as double,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DimensionsModelCopyWith<$Res>
    implements $DimensionsModelCopyWith<$Res> {
  factory _$$_DimensionsModelCopyWith(
          _$_DimensionsModel value, $Res Function(_$_DimensionsModel) then) =
      __$$_DimensionsModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double width, double height});
}

/// @nodoc
class __$$_DimensionsModelCopyWithImpl<$Res>
    extends _$DimensionsModelCopyWithImpl<$Res, _$_DimensionsModel>
    implements _$$_DimensionsModelCopyWith<$Res> {
  __$$_DimensionsModelCopyWithImpl(
      _$_DimensionsModel _value, $Res Function(_$_DimensionsModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? width = null,
    Object? height = null,
  }) {
    return _then(_$_DimensionsModel(
      width: null == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as double,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_DimensionsModel extends _DimensionsModel {
  const _$_DimensionsModel({this.width = 0.0, this.height = 0.0}) : super._();

  @override
  @JsonKey()
  final double width;
  @override
  @JsonKey()
  final double height;

  @override
  String toString() {
    return 'DimensionsModel(width: $width, height: $height)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DimensionsModel &&
            (identical(other.width, width) || other.width == width) &&
            (identical(other.height, height) || other.height == height));
  }

  @override
  int get hashCode => Object.hash(runtimeType, width, height);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DimensionsModelCopyWith<_$_DimensionsModel> get copyWith =>
      __$$_DimensionsModelCopyWithImpl<_$_DimensionsModel>(this, _$identity);
}

abstract class _DimensionsModel extends DimensionsModel {
  const factory _DimensionsModel({final double width, final double height}) =
      _$_DimensionsModel;
  const _DimensionsModel._() : super._();

  @override
  double get width;
  @override
  double get height;
  @override
  @JsonKey(ignore: true)
  _$$_DimensionsModelCopyWith<_$_DimensionsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
