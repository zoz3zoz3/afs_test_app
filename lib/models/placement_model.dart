
import 'package:freezed_annotation/freezed_annotation.dart';

part 'placement_model.freezed.dart';

@freezed
class PlacementModel with _$PlacementModel {
  const factory PlacementModel({
    @Default(0.0) double top,
    @Default(0.0) double left,
  }) = _PlacementModel;

  const PlacementModel._();
}
