import 'package:afs_test_app/models/clickable_image_model.dart';
import 'package:afs_test_app/models/dimensions_model.dart';
import 'package:afs_test_app/utils/aspect_ratio_generator.dart';
import 'package:afs_test_app/utils/color_generator.dart';
import 'package:afs_test_app/utils/count_generator.dart';
import 'package:afs_test_app/utils/layout.dart';
import 'package:afs_test_app/utils/uuid_generator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mobx/mobx.dart';

part 'image_clicker_viewmodel.g.dart';

class ImageClickerViewmodel extends _ImageClickerViewmodelBase with _$ImageClickerViewmodel {
  ImageClickerViewmodel({
    required int initialImagesCount,
  }) : super(
          initialImagesCount,
        );
}

abstract class _ImageClickerViewmodelBase with Store {
  _ImageClickerViewmodelBase(
    this.initialImagesCount,
  ) {
    initializeImages();
  }

  int initialImagesCount;

  @action
  void clickImage(String id) {
    ClickableImageModel oldImage = images.firstWhere((it) => it.id == id);
    images.removeWhere((it) => it.id == id);
    ClickableImageModel newImage = oldImage.copyWith(clicksCount: oldImage.clicksCount + 1);
    images.add(newImage);
    images = ObservableList.of(layoutImages(images.toList()));
  }

  @observable
  ObservableList<ClickableImageModel> images = ObservableList.of([]);

  @observable
  FrameSize size = FrameSize(1.sw, 1.sh);

  @computed
  double get totlaScreenArea => size.width * size.height;

  @computed
  double get totlaImgesArea => images.calculateTotalImagesArea();

  @computed
  bool get isTotalScreenAreaEqualsImagesArea => totlaImgesArea == totlaScreenArea;

  @action
  initializeImages() {
    List<ClickableImageModel> initialImages = [];
    final colors = ColorGenerator.generateUniqueColors(initialImagesCount);
    for (var i = 0; i < initialImagesCount; i++) {
      final clicks = CountGenerator.generateRandomCount();

      final aspectRatioModel = AspectRatioGenerator.generateAspectRatio();

      initialImages.add(
        ClickableImageModel(
          id: UUIDGen.generateUniqueID(),
          aspectRatioModel: aspectRatioModel,
          clicksCount: clicks,
          color: colors[i],
        ),
      );
    }

    images = ObservableList.of(layoutImages(initialImages));
  }

  List<ClickableImageModel> layoutImages(List<ClickableImageModel> images) {
    final int totalClicks = images.calculateTotalClicksCount();
    final oneClickArea = calculateClickAreaUnit(size, totalClicks);

    final imagesWithDimensions = setDimensions(oneClickArea, images);

    return positionImagesTreeStyle(imagesWithDimensions, size.width + 10, size.height);
  }
}
