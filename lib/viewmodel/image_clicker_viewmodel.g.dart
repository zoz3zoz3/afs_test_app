// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_clicker_viewmodel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ImageClickerViewmodel on _ImageClickerViewmodelBase, Store {
  Computed<double>? _$totlaScreenAreaComputed;

  @override
  double get totlaScreenArea => (_$totlaScreenAreaComputed ??= Computed<double>(
          () => super.totlaScreenArea,
          name: '_ImageClickerViewmodelBase.totlaScreenArea'))
      .value;
  Computed<double>? _$totlaImgesAreaComputed;

  @override
  double get totlaImgesArea =>
      (_$totlaImgesAreaComputed ??= Computed<double>(() => super.totlaImgesArea,
              name: '_ImageClickerViewmodelBase.totlaImgesArea'))
          .value;
  Computed<bool>? _$isTotalScreenAreaEqualsImagesAreaComputed;

  @override
  bool get isTotalScreenAreaEqualsImagesArea =>
      (_$isTotalScreenAreaEqualsImagesAreaComputed ??= Computed<bool>(
              () => super.isTotalScreenAreaEqualsImagesArea,
              name:
                  '_ImageClickerViewmodelBase.isTotalScreenAreaEqualsImagesArea'))
          .value;

  late final _$imagesAtom =
      Atom(name: '_ImageClickerViewmodelBase.images', context: context);

  @override
  ObservableList<ClickableImageModel> get images {
    _$imagesAtom.reportRead();
    return super.images;
  }

  @override
  set images(ObservableList<ClickableImageModel> value) {
    _$imagesAtom.reportWrite(value, super.images, () {
      super.images = value;
    });
  }

  late final _$sizeAtom =
      Atom(name: '_ImageClickerViewmodelBase.size', context: context);

  @override
  FrameSize get size {
    _$sizeAtom.reportRead();
    return super.size;
  }

  @override
  set size(FrameSize value) {
    _$sizeAtom.reportWrite(value, super.size, () {
      super.size = value;
    });
  }

  late final _$_ImageClickerViewmodelBaseActionController =
      ActionController(name: '_ImageClickerViewmodelBase', context: context);

  @override
  void clickImage(String id) {
    final _$actionInfo = _$_ImageClickerViewmodelBaseActionController
        .startAction(name: '_ImageClickerViewmodelBase.clickImage');
    try {
      return super.clickImage(id);
    } finally {
      _$_ImageClickerViewmodelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic initializeImages() {
    final _$actionInfo = _$_ImageClickerViewmodelBaseActionController
        .startAction(name: '_ImageClickerViewmodelBase.initializeImages');
    try {
      return super.initializeImages();
    } finally {
      _$_ImageClickerViewmodelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
images: ${images},
size: ${size},
totlaScreenArea: ${totlaScreenArea},
totlaImgesArea: ${totlaImgesArea},
isTotalScreenAreaEqualsImagesArea: ${isTotalScreenAreaEqualsImagesArea}
    ''';
  }
}
