import 'package:afs_test_app/models/clickable_image_model.dart';
import 'package:flutter/material.dart';

class ImageWidget extends StatefulWidget {
  const ImageWidget({
    super.key,
    required this.model,
    required this.index,
    required this.onTab,
  });
  final ClickableImageModel model;
  final VoidCallback onTab;
  final int index;
  @override
  State<ImageWidget> createState() => _ImageWidgetState();
}

class _ImageWidgetState extends State<ImageWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTab,
      child: AnimatedContainer(
        decoration: BoxDecoration(
          color: widget.model.color,
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
        ),
        padding: const EdgeInsets.all(5),
        width: widget.model.dimensionsModel.width,
        height: widget.model.dimensionsModel.height,
        duration: const Duration(milliseconds: 200),
        child: FittedBox(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('count: ${widget.model.clicksCount.toString()}'),
              Text(
                  'aspect ratio: ${widget.model.aspectRatioModel.aspectRatioX.toString()} : ${widget.model.aspectRatioModel.aspectRatioY.toString()}'),
              Text('area: ${widget.model.dimensionsModel.area.toStringAsFixed(2)}'),
              Text('index: ${widget.index.toString()}'),
            ],
          ),
        ),
      ),
    );
  }
}
