class Constants {
  static const String characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  static const int uuidLength = 8;

  static const int initialCountGeneratorMinValue = 10;
  static const int initialCountGeneratorMaxValue = 15;

  static const List<Map<int, int>> ratiosMap = [
    {1: 1},
    {3: 2},
    {4: 3},
  ];

  static const clickRatio = 8;
}
