import 'dart:math';

import 'package:afs_test_app/utils/constants.dart';

class UUIDGen {
  static String generateRandomString(int length) {
    final random = Random();
    return List.generate(length, (index) => Constants.characters[random.nextInt(Constants.characters.length)]).join();
  }

  static String generateUniqueID() {
    final timestamp = DateTime.now().millisecondsSinceEpoch;
    final randomPart = generateRandomString(Constants.uuidLength);
    return '$timestamp-$randomPart';
  }
}
