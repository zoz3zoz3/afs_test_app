import 'dart:math';

import 'package:afs_test_app/models/dimensions_model.dart';
import 'package:afs_test_app/utils/constants.dart';

class AspectRatioGenerator {
  static AspectRatioModel generateAspectRatio() {
    final random = Random();

    Map<int, int> aspectRatio = Constants.ratiosMap[random.nextInt(Constants.ratiosMap.length)];

    return AspectRatioModel(
      aspectRatioX: aspectRatio.keys.first,
      aspectRatioY: aspectRatio.values.first,
    );
  }
}
