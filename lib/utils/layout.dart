import 'dart:math';

import 'package:afs_test_app/models/clickable_image_model.dart';
import 'package:afs_test_app/models/dimensions_model.dart';
import 'package:afs_test_app/models/placement_model.dart';


double calculateClickAreaUnit(FrameSize size, int totalClicks) {
  double screenArea = size.width * size.height;

  return screenArea / totalClicks;
}

List<ClickableImageModel> setDimensions(double clickArea, List<ClickableImageModel> images) {
  images = images.map((im) {
    final imageArea = im.clicksCount * clickArea;
    im = im.copyWith(dimensionsModel: calculateDimensions(imageArea, im.aspectRatioModel.aspectRatio));
    return im;
  }).toList();
  return images;
}

DimensionsModel calculateDimensions(double area, double aspectRatio) {
  double height = sqrt(area / aspectRatio);
  double width = aspectRatio * height;
  return DimensionsModel(width: width, height: height);
}

List<ClickableImageModel> positionImagesTreeStyle(
    List<ClickableImageModel> images, double screenWidth, double screenHeight) {
  List<ClickableImageModel> sortedImages = images.sortListByArea();
  List<ClickableImageModel> placedImages = [];

  void placeImageAndChildren(ClickableImageModel parentImage) {
    while (sortedImages.isNotEmpty) {
      bool imagePlaced = false;
      for (int i = 0; i < sortedImages.length; i++) {
        ClickableImageModel currentImage = sortedImages[i];
        double parentRightEdge = parentImage.placementModel.left + parentImage.dimensionsModel.width;
        double parentBottomEdge = parentImage.placementModel.top + parentImage.dimensionsModel.height;
        double newLeft = parentRightEdge;
        double newTop = parentImage.placementModel.top; // parentBottomEdge - currentImage.dimensionsModel.height;

        if (newLeft + currentImage.dimensionsModel.width <= screenWidth) {
          // Image fits to the right
          sortedImages.removeAt(i);
          placedImages.add(currentImage.copyWith(placementModel: PlacementModel(top: newTop, left: newLeft)));
          imagePlaced = true;
          break;
        } else if (i == 0) {
          // The large image doesn't fit to the right, try to place it below
          newLeft = 0;
          newTop = parentBottomEdge;
          // if (newTop + currentImage.dimensionsModel.height <= screenHeight) {
          sortedImages.removeAt(i);
          placedImages.add(currentImage.copyWith(placementModel: PlacementModel(top: newTop, left: newLeft)));
          imagePlaced = true;
          break;
          // }
        }
      }

      if (!imagePlaced) {
        break;
      }

      if (placedImages.isNotEmpty) {
        placeImageAndChildren(placedImages.last);
      }
    }
  }

  // Start
  ClickableImageModel rootImage =
      sortedImages.removeAt(0).copyWith(placementModel: const PlacementModel(top: 0, left: 0));
  placedImages.add(rootImage);

  placeImageAndChildren(rootImage);

  return placedImages;
}

List<ClickableImageModel> positionImagesBinPack(List<ClickableImageModel> images, double screenWidth) {
  List<ClickableImageModel> sortedImages = images.sortListByArea();
  List<ClickableImageModel> placedImages = [];
  List<ClickableImageModel> toBePlacedImages = [];

  double currentX = 0.0;
  double currentY = 0.0;
  double nextY = 0.0; //
  double currentRowMaxHeight = 0.0;

  bool tryPlaceImage(ClickableImageModel image, bool canRotate) {
    double width = image.dimensionsModel.width;
    double height = image.dimensionsModel.height;
    bool rotated = false;

    if (currentX + width > screenWidth &&
        canRotate &&
        height <= screenWidth - currentX &&
        width <= currentRowMaxHeight) {
      rotated = true;
      width = image.dimensionsModel.height;
      height = image.dimensionsModel.width;
    }

    if (currentX + width <= screenWidth) {
      placedImages.add(image.copyWith(
          dimensionsModel:
              rotated ? image.dimensionsModel.copyWith(width: width, height: height) : image.dimensionsModel,
          placementModel: PlacementModel(top: currentY, left: currentX)));
      currentX += width;
      nextY = max(nextY, currentY + height);
      currentRowMaxHeight = max(currentRowMaxHeight, height);
      return true;
    }

    return false;
  }

  for (var image in sortedImages) {
    if (!tryPlaceImage(image, true)) {
      currentX = 0.0;
      currentY = nextY;
      currentRowMaxHeight = 0.0;

      if (!tryPlaceImage(image, false)) {
        toBePlacedImages.add(image);
      }
    }
  }

  for (ClickableImageModel image in toBePlacedImages) {
    if (!tryPlaceImage(image, true)) {
      print("Could not place image with id: ${image.id}");
    }
  }

  return placedImages;
}

// PlacementModel findPosition(double parentRightEdge, double parentBottomEdge, DimensionsModel imageDimensions) {
//   double top, left;

//   if (parentRightEdge + imageDimensions.width <= screenWidth) {
//     left = parentRightEdge;
//     top = parentBottomEdge - imageDimensions.height;
//   } else {
//     left = 0;
//     top = parentBottomEdge;
//   }

//   return PlacementModel(top: top, left: left);
// }
