import 'dart:math';

import 'package:afs_test_app/utils/constants.dart';

class CountGenerator {
  static int generateRandomCount() {
    final random = Random();
    return Constants.initialCountGeneratorMinValue +
        random.nextInt(Constants.initialCountGeneratorMaxValue - Constants.initialCountGeneratorMinValue + 1);
  }

  static int generateRandomInt(int min, int max) {
    final random = Random();
    return min + random.nextInt(max - min + 1);
  }

  static double generateRandomDouble(double max) {
    final random = Random();
    return 0.1 + random.nextDouble();
  }
}
