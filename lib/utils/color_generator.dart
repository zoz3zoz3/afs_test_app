import 'dart:math';

import 'package:flutter/material.dart';

class ColorGenerator {
  static Color generateRandomColor() {
    Random random = Random();
    int r = random.nextInt(256);
    int g = random.nextInt(256);
    int b = random.nextInt(256);
    return Color.fromARGB(255, r, g, b);
  }

  static List<Color> generateUniqueColors(int count) {
    Set<Color> uniqueColors = {};
    while (uniqueColors.length < count) {
      uniqueColors.add(generateRandomColor());
    }
    return uniqueColors.toList();
  }
}
