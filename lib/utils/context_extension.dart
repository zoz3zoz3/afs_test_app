import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension ContextExtension on BuildContext {
  //* THEME STUFF *//
  ThemeData get theme => Theme.of(this);

  TextTheme get textTheme => theme.textTheme;

  //* Media query stuff *//
  MediaQueryData get mediaQuery => MediaQuery.of(this);

  double get fullWidth => mediaQuery.size.width;

  double get fullHeight => mediaQuery.size.height;

  //* Navigate stuff *//
  NavigatorState get navigator => Navigator.of(this);

  void pop<T extends Object>([T? result]) => navigator.pop<T>(result);

  void popUntil({numberOfPages = 1}) {
    int count = 0;
    navigator.popUntil((_) => count++ == numberOfPages);
  }

  Future<T?> pushNamed<T extends Object?>(
    String routeName, {
    Object? arguments,
  }) =>
      navigator.pushNamed<T>(routeName, arguments: arguments);

  Future<T?> pushPage<T extends Object>(Widget widget) => navigator.push<T>(MaterialPageRoute(builder: (_) => widget));

  Future<T?> cupertinoPushPage<T extends Object>(Widget widget) =>
      navigator.push<T>(CupertinoPageRoute(builder: (_) => widget));

  double get deviceWidth {
    final screenSize = MediaQuery.of(this).size;
    return screenSize.width;
  }

  Orientation get deviceOrientation => MediaQuery.of(this).orientation;

  bool get isIpad => MediaQuery.of(this).size.shortestSide >= 600;
}

