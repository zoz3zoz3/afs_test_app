import 'package:afs_test_app/viewmodel/image_clicker_viewmodel.dart';
import 'package:get_it/get_it.dart';

final GetIt sl = GetIt.I;

Future<void> inject() async {
  injectMobx();
  injectDataSources();
  injectRepositories();
  injectInteractors();
}

void injectMobx() {
  sl.registerFactoryParam<ImageClickerViewmodel, int, void>(
    (param1, param2) => ImageClickerViewmodel(
      initialImagesCount: param1,
    ),
  );
}

void injectDataSources() {}
void injectRepositories() {}
void injectInteractors() {}
