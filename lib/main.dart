import 'package:afs_test_app/di/injection_container.dart';
import 'package:afs_test_app/pages/choose_images_count.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() async {
  await inject();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(428, 926),
      minTextAdapt: true,
      builder: (context, child) => MaterialApp(
        title: 'AFS Test App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const ChooseImagesCount(),
      ),
    );
  }
}
